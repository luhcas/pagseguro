<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1 class="titulos">Pagamento via api estruturada do PagSeguro</h1>
    <hr class="subtitulo">
    <br><br><br>
    <p>Simulação de Compra</p>
    <p>Valor a ser pago: R$ 10,00</p>
    <p>Dados do comprador de teste</p>
    <p>Email: c91654887301508345666@sandbox.pagseguro.com.br</p>
    <p>Senha: h5602513R66442B2</p>
    <p>Cartão de teste</p>
    <p>Número: 4111111111111111 --- Bandeira: VISA --- Válido até: 12/2030 --- CVV: 123</p>
    <p>CPF de teste: 08987743659</p>
    <button class="btn btnConfirma" onclick="enviaPagseguro()">Comprar</button>
    <!-- action oficial - https://pagseguro.uol.com.br/checkout/v2/payment.html -->
    <form id="comprar" action="https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">

    <input type="hidden" name="code" id="code" value="" />
    <script type="text/javascript">
    function enviaPagseguro(codigo){
      $.post('salvarpedido.php','',function(idPedido){
        $.post('pagseguro.php',{idPedido: idPedido},function(data){

          $('#code').val(data);
          console.log(data);
          $('#comprar').submit();
          })
        })
      }
    </script>

    <script src="jquery_pagseguro.js"></script>
    <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>


  </body>
</html>
